
import csv
import random
import tkinter as tk
import PIL 
from PIL import Image, ImageTk 

from random import randint

def chargerDonnees(nomFichier):
    """
    Ouverture du fichier csv afin d'en extraire les données d'entrainements sportifs
    :param nomFichier: Nom du fichier csv traité, ici nsicsv.csv
    :return: retourne les informations sous forme de dictionnaire traitable
    """
    dico = {}
    with open(nomFichier, newline='') as csvfile:
        contenu = csv.DictReader(csvfile, delimiter=',')
        for row in contenu:
            
            if row["sport"] not in dico:
                dico[row["sport"]] = [[row["exercice"], row["mobilite"], row["intensité"]]]
            else:
                dico[row["sport"]].append([row["exercice"], row["mobilite"], row["intensité"]])
    return dico

def selectionnerEnchainement(mobilite, intensite, sport, dictionnaire): 
    """
    Sélectionne un enchaînement de 4 mouvements en fonction des critères de mobilité, d'intensité et de sport.
    :param mobilite: la mobilité choisie par l'utilisateur
    :param intensite: L'intensité choisie
    :param sport: Le sport pratiqué
    :param dictionnaire: dictionnaire contenant les données d'entraînement
    :return: Une liste de mouvements formant un enchaînement
    """

    enchainement = []
    intensiteMax = intensite * 3 + 1
    mobiliteMax = mobilite * 3 + 1

    mouvementsPossibles = []
    for mouvement in dictionnaire[sport]: 
        if int(mouvement[1]) <= mobiliteMax:
            mouvementsPossibles.append(mouvement)

    while len(enchainement) < 3:
        mouvement = random.choice(mouvementsPossibles)
        if sum([int(m[2]) for m in enchainement]) + int(mouvement[2]) <= intensiteMax:
            enchainement.append(mouvement)

    if sum([int(i[1]) for i in enchainement]) > mobiliteMax:
        while sum([int(i[1]) for i in enchainement]) > mobiliteMax:
            enchainement.pop()
    return enchainement

def passerFenetre(fenetre):
    """

    :param fenetre: la fenêtre d'accueil
    :return: ferme la fenêtre
    """
 
    fenetre.destroy()

def passerProchaineFenetre(sport, mobilite, intensite, fenetre):
    """

    :param sport: indique à l'utilisateur son choix de sport
    :param mobilite: indique à l'utilisateur son choix de mobilité
    :param intensite: indique à l'utilisateur son choix d'intensité
    :param fenetre: lance passerFenetre, fermant la fenetre d'accueil ici en paramètre
    :return: Rappelle les choix de l'utilisateur, et ferme la fenetre initiale d'accueil
    """
    print("Sport choisi:", sport)
    print("Mobilité choisie:", mobilite)
    print("Intensité choisie:", intensite)
    
    passerFenetre(fenetre)

def interface():
    """

    :return: Crée la fenetre d'accueil dans laquelle se trouvent trois menus déroulants
    """

    # Création de la fenêtre principale
    fenetre = tk.Tk()
    fenetre.geometry("900x600")
    fenetre.title("Sélection de sport")

    # Création des menus déroulants
    sports = tk.StringVar(fenetre)
    sports.set("Sport pratiqué")
    menuSports = tk.OptionMenu(fenetre, sports, "Football", "Basket-ball", "Volley-ball", "Tennis", "Course à pied", 
                               "Escalade", "Natation", "Natation synchronisée", "Aucun sport")
    menuSports.grid(row=0, column=0)

    mobilitetexte = tk.IntVar(fenetre)
    mobilitetexte.set("Niveau de mobilité")
    menuMobilite = tk.OptionMenu(fenetre, mobilitetexte, 1, 2, 3)
    menuMobilite.grid(row=3, column=0)

    intensitetexte = tk.IntVar(fenetre)
    intensitetexte.set("Niveau d'intensité")
    menuIntensite = tk.OptionMenu(fenetre, intensitetexte, 1, 2, 3)
    menuIntensite.grid(row=4, column=0)

    boutonSuivant = tk.Button(fenetre, text="Générer un enchainement",
                              command=lambda: passerProchaineFenetre(sports.get(), mobilitetexte.get(),
                                                                     intensitetexte.get(), fenetre))
    boutonSuivant.grid(row=5, column=0)

    # Lancement de la boucle principale

    fenetre.mainloop()
    return(sports.get(), mobilitetexte.get(), intensitetexte.get()) 
    
def main():
    """

    :return: Lance la fonctions charger données pour mettre les données du fichier csv sous forme d'un dictionnaire, puis
    """
    donnees = chargerDonnees("trainme.csv") 

    sportChoisi = ''
    mobiliteChoisie = 0
    intensiteChoisie = 0
    sportChoisi, mobiliteChoisie, intensiteChoisie = interface()
    enchainement = selectionnerEnchainement(mobiliteChoisie, intensiteChoisie, sportChoisi, donnees)
    afficherEnchainement(enchainement)
    descriptifVisuel(enchainement)

def afficherEnchainement(enchainement):
    """

    :param enchainement: l'enchainement généré par le programme
    :return: affiche chaque mouvement, la mobilité et l'intensité correspondante
    """

    print("Enchaînement sélectionné :")
    for mouvement in enchainement:
        print("- Mouvement : {}, Mobilité : {}, Intensité : {}".format(mouvement[0], mouvement[1], mouvement[2]))####The index starts with 0

def affichages(text, image):
    """

    :param text: Dans le dictionnaire conditions, prend selon le mouvement une phrase qui la decrit et l'insere dans une fenetre
    :param image: Dans le dictionnaire conditions, prend selon le mouvement une image correspondante
    :return: Retourne une fenetre contenant du texte ainsi qu'une image qui explique chauqe mouvement à l'utilisateur
    """
    root = tk.Tk()
    root.geometry("900x600")
    root.title("Affichage de l'enchainement")
    label = tk.Label(root, text=text, bg="white", fg="black", 
                     font=("Helvetica", 16, "bold"))
    label.pack(pady=10, padx=10)
    img= Image.open("photos_nsi/" + image)
    img = img.convert(mode="RGBA")
    img = ImageTk.PhotoImage(img)
    panel = tk.Label(root, image=img, bg="white")
    panel.image = img
    panel.configure(image=img)
    panel.pack(side="bottom", fill="both", expand="yes")
    root.mainloop()


# Création d'un dictionnaire d'association avec pour clés les mouvements possibles, et en valeurs une fonction affichant une fenêtre correspondante
conditions = {"Flexions et extensions des doigts" : lambda :affichages("Effectuez des flexions et extensions de vos doigts" , "stockvault-man-stretching-silhouette253298.jpg"),
              "Marche légère" : lambda : affichages("Faites deux minutes de marche légère" , "marche+course lente.jpg"),
              "Course lente" : lambda : affichages("Effectuez deux minutes de course lente" , "marche+course lente.jpg"),
              "Talons-fesses": lambda : affichages("Effectuez deux minutes de talons-fesses" , "talons_fesses.png"),
              "Montées de genoux": lambda : affichages("Effectuez deux minutes de montées de genoux" , "montée de genoux.png"),
              "Etirements des jambes" : lambda : affichages("Effectuez des étirements dynamiques des jambes" , "silhouette-3098251_1280.png"),
              "Foulées bondissantes": lambda : affichages("Effectuez deux minutes de foulées bondissantes","sauts.png"),
              "Pas chassés":  lambda : affichages("Effectuez deux minutes de pas chassés","stockvault-man-stretching-silhouette253298.jpg"),
              "Skipping": lambda : affichages("Effectuez deux minutes de skipping avec une corde à sauter","stockvault-man-stretching-silhouette253298.jpg"),
              "Accélération progressive": lambda : affichages("Effectuez des aller-retours de course en accélérant progressivement pendant deux minutes","course_rapide+sprints.jpg"),
              "Squats" : lambda : affichages("Effectuez deux minutes de squats légers","squat légers.png"),
              "Battements de jambes" : lambda : affichages("Effectuez deux minutes de battements de jambes","batements de jambe.png"),
              "Bras circulaires" : lambda : affichages("Faites des rotations cirulaires des bras pendant une minute","étirements bras et jambes.jpg"),
              "Plongeons": lambda : affichages("Effectuez cinq plongeons d'entrainement","natation.png"), 
              "Nage sur le dos" : lambda : affichages("Nagez sur le dos sur 50m","natation.png"),
              "Nage sur le ventre": lambda : affichages("Nagez sur le ventre sur 50m","natation.png"),
              "Brasses": lambda : affichages("Effectuez deux longueurs de brasse","natation.png"),
              "Crawl": lambda : affichages("Effectuez deux longueurs de crawl","natation.png"),
              "Papillon": lambda : affichages("Effectuez deux longueurs de papillon","natation.png"),
              "Course autour du terrain": lambda : affichages("Courez autour du terrain pendant trois minutes","course_rapide+sprints.jpg"),
              "Passes de ballon": lambda : affichages("Effectuez deux minutes de passes de ballon avec un ami ou contre un mur","football (2).png"),
              "Frappes de ballon": lambda : affichages("Effectuez des frappes de ballon répétées pendant trois minutes","football (2).png"),
              "Changements de direction rapides": lambda : affichages("Entrainez-vous à effectuer des changements rapides de direction en courant pendant trois minutes","man-308894_1280.png"),
              "Sprints": lambda : affichages("Effectuez deux minutes de sprints intensifs","course_rapide+sprints.jpg"),
              "Passe et va": lambda : affichages("Effectuez des passes pendant deux minutes","basket.png"),
              "Tirs au panier": lambda : affichages("Effectuez des tirs au panier pendant trois minutes","basket.png"),
              "Etirements des bras et des épaules" : lambda : affichages("Effectuez des étirements des bras et des épaules pendant deux minutes","étirements bras et jambes.jpg"),
              "Circulations articulaires": lambda : affichages("Effectuez un échauffement de vos articulations pendant deux minutes","étirements bras et jambes.jpg"),
              "Etirements des bras et des poignets": lambda : affichages("Effectuez un échauffement de vos bras et de vos poignets pendant deux minutes","étirements bras et jambes.jpg"),
              "Moulinets avec la raquette": lambda : affichages("Effectuez des moulinets avec la raquette pendant deux minutes","service tennis etc.png"),
              "Exercices de déplacement latéral" : lambda : affichages("Effectuez des déplacements latéraux pendant trois minutes","marche+course lente.jpg"),
              "Coup droit et revers en mouvement": lambda : affichages("Effectuez des coups droits et des revers pendant trois minutes","service tennis etc.png"),
              "Sauts en place": lambda : affichages("Effectuez des sauts sur place pendant deux minutes","sauts.png"),
              "Entraînement au service": lambda : affichages("Entrainez-vous au service pendant trois minutes","service tennis etc.png"),
              "Mouvements de rotation du tronc": lambda : affichages("Effectuez des mouvements de rotation du tronc","mouvement de rotation du tronc.png"),
              "Etirements dynamiques des épaules": lambda : affichages("Effectuez des étirements des épaules","étirements bras et jambes.jpg"),
              "Figures de base en mouvement": lambda : affichages("Effectuez des figures de bases en mouvement pendant trois minutes","natation.png"),
              "Respiration profonde": lambda : affichages("Effectuez des respirations profondes pendant deux minutes","respiration profonde.png"),
              "Mouvements légers de déplacement dans l'eau": lambda : affichages("Effectuez des mouvements de déplacements dans l'eau pendant trois minutes","natation.png"),
              "Flexions des chevilles": lambda : affichages("Effectuez des flexions de chevilles pendant deux minutes","étirements bras et jambes.jpg"),
              "Echauffement des muscles du haut du corps": lambda : affichages("Effectuez des mouvements d'échauffement du haut du corps","stockvault-man-stretching-silhouette253298.jpg"),
              "Echauffement des muscles du bas du corps": lambda : affichages("Effectuez des mouvements d'échauffement du bas du corps","stockvault-woman-running-silhouette255158.jpg"),
              "Simulations de grimpe": lambda : affichages("Effectuez des simulations de grimpe pendant trois minutes","fentes latérales.png"),
              "Pompes": lambda : affichages("Effectuez des pompes pendant deux minutes","pompes.png"),
              "Marche rapide": lambda : affichages("Effectuez une marche rapide pendant deux minutes","marche+course lente.jpg"),
              "Simulations de grimpe": lambda : affichages("Effectuez des simulations de grimpe pendant trois minutes","simulation d'escalade.png"),
              "Fentes avant et latérales": lambda : affichages("Effectuez des fentes avant latérales pendant trois minutes","fentes latérales.png"),
              "Mountain climbers": lambda : affichages("Effectuez des moutains climbers pendant deux minutes","mountains_climbers.png"),
              "Yoga léger Un": lambda : affichages("Effectuez la figure du cobra, maintenez la position pendant quarante secondes","yoga pose cobra.jpgg"),
              "Yoga léger Deux": lambda : affichages("Effectuez la figure du chiot, maintenez la position pendant trente secondes","yoga dog pose.jpg"),
              "Yoga léger Trois": lambda : affichages("Effectuez la figure du chiot, maintenez la position pendant trente secondes","yoga puppy pose.jpg"),
              }


def descriptifVisuel(enchainement):
    """
    Affiche une description visuelle de chaque mouvement de l'enchaînement.
    :param enchainement: L'enchaînement précédemment généré.
    """
    
    for mouvement in enchainement:
        # Ajout d'options supplémentaires pour la catégorie yoga, générées aléatoirement
        if mouvement == "Yoga léger":
            number = randint(1, 3)
            if number == 1:
                mouvement = "Yoga léger Un"
            elif number == 2:
                mouvement = "Yoga léger Deux"
            else:
                mouvement = "Yoga léger Trois"

        conditions[mouvement[0]]()

main()
